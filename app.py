""" This is the Groupme bot message handler"""
import random
import re
import sys
from flask import Flask, request
from groupbot import GroupBot
from plugins.hooky import get_hooks


# Make Flask app
APP = Flask(__name__)
# Make GroupBot
BOT = GroupBot.from_json('.bot_true.json', quiet=bool(sys.argv[1]))
hooks = get_hooks()


@APP.route('/', methods=['POST'])
def parse():
    """ Takes each message and handles if it is a trigger word"""
    data = request.json
    print(data)
    text = data["text"].lower()
    user = data["user_id"]

    for hook in hooks:
        ret = hook(BOT, data)
        if ret is 0:
            return

    if re.match("!shutdown", text):
        BOT.blast_message("MikeBot shutdown...")
        sys.exit()
        '''    elif re.search("[a-z]+ism", text):
        name = re.search(r'([a-z]+)ism', text)
        BOT.random_message_user(name.group(1))'''
    elif re.search(r".*((roll) (\d+) to (\d+))", text):
        nums = re.search(r'.*((roll) (\d+) to (\d+))', text)
        low = int(nums.group(3))
        high = int(nums.group(4))+1
        out = "Rolling from %d to %d.\nResult %d." % (low, high-1, random.randrange(low, high))
        BOT.blast_message(out)
    elif re.match("^source$", text):
        BOT.blast_message("http://bitbucket.org/aqm5498/mikebot/")
    elif re.match("reload$", text):
        BOT.load_members()
    elif re.match("patchnotes", text):
        with open('patch_note.txt', 'r') as note:
            txt = note.read()
            BOT.blast_message(txt)
    else:
        try:
            BOT.log_user_message(data)
        except (IOError, NameError) as er:
            print(er)

    return "GOTEM"


if __name__ == "__main__":
    APP.run(host='0.0.0.0', port=5000)
