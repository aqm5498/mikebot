v1.0.4
+ Added roll feature to the bot, include the text roll # to # in your message for a result.
+ Improved the way logging happens so blank lines are no longer possible to randomly draw
+ Quiet mode enabled for reloading for daily message updates
+ Minor bug fixes - Luckily no one finds them
