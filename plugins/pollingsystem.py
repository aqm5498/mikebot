""" Polling system plugin for GroupMeBot """
import re
import threading

TIME_VOTE = 180

def on_message(bot, msg):
    """ Handles all messages that deal with polling system"""
    if msg.get('user_id', 0) is bot.bot_user_id:
        return -1

    text = msg.get('text', '')
    if re.search(r'\bpoll (.+\?)', text):
        lines = text.splitlines()
        if len(lines) < 2:
            bot.blast_message(
                "Please place options on another line\nE.g poll question?\nOptions: 1,3,4,5")
        question = re.findall(r'\bpoll (.+\?)', lines[0])
        options = re.search(r'\boptions: (.+)', lines[1].lower())

        options = [option.strip() for option in options.group(1).split(',')]

        if options and question:
            poll(bot, question[0], options)
            return 0
        return -1
    elif re.search(r'\bvote (.+)', text):
        vote = re.findall(r'\bvote (\b\w+\b)', text)[0]
        if bot.polling:
            enter_vote(bot, msg.get('user_id'), vote)
        return 0
    else:
        return -1


def poll(bot, question, opts):
    """ Starts a poll for question q with options o"""
    text = "Voting started for: {}\n Options are {}".format(question, str(opts))
    text += "\nVoting closes in {} minutes!".format(TIME_VOTE)
    bot.blast_message(text)
    bot.polling = True
    bot.poll_results = {x:0 for x in opts}
    bot.user_voted = []
    bot.poll_count = 0
    #threading.Timer(TIME_VOTE-60, bot.blast_message, ["1 Minute
    #remains!"]).start()
    threading.Timer(TIME_VOTE-10, print, ["1 Minute remains!"]).start()
    threading.Timer(TIME_VOTE, close_poll, kwargs={'bot':bot}).start()



def enter_vote(bot, user, vote):
    """ Enters user's vote into poll """
    if user in bot.user_voted:
        return
    vote = vote.strip()
    try:
        bot.poll_results[vote] += 1
        bot.poll_count += 1
        bot.user_voted.append(user)
        name = bot.memlist[user]
        name = "{}{}".format(name[0].upper(), name[1:])
        bot.blast_message("{}'s vote was recorded.".format(name))
    except KeyError:
        bot.blast_message('That wasn\'t an option, {}{}'.format(
            bot.memlist[user][0].upper(), bot.memlist[user][1:]))


def close_poll(bot):
    """ Tally results and submit them to chat """
    text = "Poll Results"
    for k in bot.poll_results.keys():
        pct = float(bot.poll_results[k]/bot.poll_count)
        text += '\n{}: {:.2%}'.format(k, pct)

    bot.polling = False
    bot.blast_message(text)
