""" Nameism Plugin for GroupMeBot

    This module takes the users name and returns a random utterance form that user
"""
import random
import re


def on_message(bot, msg):
    """ Sends a string utterance from user requested to the GroupMe """
    if not bot:
        return

    msg = msg.get('text', '').lower()
    names = re.findall(r'\b(\w+)ism', msg)
    if not names:
        return -1

    for name in names:
        lines = bot.people_lines.get(name, [])
        if lines:
            target = name
            break

    if not lines:
        return -1

    num = random.randrange(0, len(lines))
    line = lines[num]

    output_msg = "{}{} once said: {}".format(target[0].upper(), target[1:], line)

    bot.blast_message(output_msg)

    return 0
