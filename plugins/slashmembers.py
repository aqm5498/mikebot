""" /Members plugin for GroupMeBot

    This module prints all users and ids to the chat
"""
import re

def on_message(bot, msg):
    """ Sends a message containing all users id's and names in the chat"""
    if not re.match('/members$', msg.get('text','').lower()):
        return -1

    if not bot.memlist:
        bot.load_members()

    text = ""
    print("Memlist is %s long" % len(bot.memlist))
    for k, value in bot.memlist.items():
        print("%s + %s" % (k, value))
        text = text + str(k) + " : " + str(value) + "\n"

    bot.blast_message(text)

    return 0
