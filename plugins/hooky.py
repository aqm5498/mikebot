"""Get Hooks for Chat bot from plugins dir

    Any file ending in .py in the plugins directory with a on_message function
    are automatically added to the hook list Hook list is returned to be looped
    over in main program

"""
import importlib
import os
import re


def get_hooks(plugin_dir="plugins"):
    """Gets all hooks from the plugins directory and returns handles

        get_hooks(plugin_dir="pathtoplugindir") may be used if the default
    ./plugins/ is not suitable.

    """
    plugin_path = os.path.join(os.getcwd(), plugin_dir)
    plugin_mods = [os.path.basename(plug) for plug in os.listdir(plugin_path)
                   if plug.endswith('.py')]

    hook_list = []

    for plugin in plugin_mods:
        mod = importlib.import_module("{}.{}".format(plugin_dir, plugin[:-3]))
        for hook in re.findall(r'on_message', ' '.join(dir(mod))):
            func = getattr(mod, "on_message")
            hook_list.append(func)

    return hook_list

if __name__ == "__main__":
    TEST = get_hooks()

    for t in TEST:
        t('print')
