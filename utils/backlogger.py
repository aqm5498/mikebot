import json
import requests
import math
from groupbot import GroupBot
from .groupmeapi import *
import re

r1 = re.compile("^fuck mike$")
r2 = re.compile("^/members$")
r3 = re.compile("([a-z]+)ism")
r4 = re.compile("^source$")

rules = [r1, r2, r3, r4]

grp = GroupBot.from_json('.bot_true.json', quiet=True)

def get_all_messages(logfile):
    messages_json = api_get_request("/messages", gid=grp.gid, token=grp.token, limit=100)
    queries = math.ceil(int(messages_json["response"]["count"])/ 100.0)
    message_log = {"messages" : []}


    for _ in range(queries):
        messages = messages_json["response"]["messages"]
        message_log["messages"] += messages
        earliest_msg = messages[len(messages)-1]["id"]

        #query for messages before last id
        req = "100&before_id="+str(earliest_msg)
        messages_json = api_get_request("/messages", gid=grp.gid, token=grp.token,
                limit="100", before_id=str(earliest_msg))

        if messages_json is None:
            break

    with open(logfile, 'w') as f:
        json.dump(message_log, f, sort_keys=True, indent=4)


def save_messages_from_log(logfile):
    with open(logfile, 'r') as f:
        msgs = json.load(f)
    msgs = msgs["messages"]

    #load members
    with open("data/members.json", 'r') as m:
        data = json.load(m)

    # Get member filenames
    userdict = {}
    for member in data["members"]:
        userdict[member["user_id"]] = {"filename": member["filename"]+".log",
                                    "text": ""}



    for msg in msgs:
        if msg["sender_id"] in userdict.keys():
            record = True
            text = ""
            if msg["text"]:
                for rule in rules:
                    if rule.search(msg["text"].lower()):
                        record = False

                if len(msg["text"].split('\n')) > 1:
                    record = False
                    line = ' '.join(msg["text"].split('\n'))
                    if line.isspace() or line is '':
                        continue
                    else:
                        text = line + '\n'

                if record:
                    text = msg["text"] + '\n'

                userdict[msg["sender_id"]]["text"] += text


    for k in userdict.keys():
        user = userdict[k]
        with open('data/'+user["filename"], 'w') as f:
            f.write(user["text"])

if __name__== "__main__":
    get_all_messages("data/message_log.json")
    save_messages_from_log("data/message_log.json")
