""" handles all get and save requests to groupme api """
import json
import requests


def api_get_request(uri=None, **kwargs):
    """ Makes request to api returns json
            Default url with no arguments /v3/groups/gid
            uri extentions after default url    - Type: string
            limit number of entries to receive  - Type: int

        """
    if "gid" in kwargs:
        host = "https://api.groupme.com/v3/groups/"+kwargs['gid']
    else:
        raise KeyError

    request_url = host if uri is None else host+str(uri)
    if "token" in kwargs:
        request_url = request_url+"?token="+kwargs["token"]

    if "limit" in kwargs:
        request_url = request_url+"&limit="+str(kwargs["limit"])

    if "before_id" in kwargs:
        request_url = request_url+"&before_id="+str(kwargs["before_id"])

    resp = requests.get(request_url)

    print("Request Status Code: %d " % resp.status_code)
    if resp.status_code != 200:
        return None
    else:
        return resp.json()

def save_result(data, outfile):
    """ Pretty Prints json data to outfile
        data is json string     - Type: String(json)
        outfile is file name    - Type: String
    """
    with open(str(outfile), 'w') as out_f:
        json.dump(data, out_f, sort_keys=True, indent=4)
