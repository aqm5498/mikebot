"""GroupBot Allows the bot to interact with the group"""
import json
import threading

import requests


class GroupBot:
    """ Handles all interactions with GroupMe Groups API"""
    def __init__(self, groupid, authtoken, bot_id, quiet=False):
        print("Seting token to %s" % authtoken)
        self.token = str(authtoken)
        print("Seting gid to %s" % groupid)
        self.gid = groupid
        self.memlist = {}
        self.people_lines = {}
        self.bot_id = str(bot_id)
        self.bot_user_id = '360538'
        self.quiet = quiet
        self.polling = False
        self.poll_count = 0
        self.poll_results = {}
        self.user_voted = []
        print("Loading Members ....")
        self.load_members()
        print("Done")

    @classmethod
    def from_json(cls, json_fp, quiet=False):
        ''' Creates a GroupBot object from json config file'''
        try:
            with open(json_fp, 'r') as cfg:
                data = json.load(cfg)
                gid = data["group_id"]
                authtoken = data["api_token"]
                bot_id = data["bot_id"]

            return cls(gid, authtoken, bot_id, quiet=quiet)
        except [FileNotFoundError, TypeError, KeyError] as err:
            print(err)
            return None

    def load_members(self):
        """ Populates member lists
            This function assumes member list is stored directly from API
            The stored file should be named members.json
        """
        parsed = None
        with open("data/members.json", 'r') as members:
            parsed = json.load(members)

        members = parsed["members"]
        loaded = ""
        for member in members:
            member_file = "data/"+member["filename"]+".log"
            self.memlist[member["user_id"]] = member["filename"]
            try:
                with open(member_file, 'r') as member_f:
                    self.people_lines[member["filename"]] = member_f.read().splitlines()
                    loaded += "%s loaded %d lines\n" % (member_file,
                                                        len(self.people_lines[member["filename"]]))
                    print("%s loaded %d lines" % (member_file,
                                                  len(self.people_lines[member["filename"]])))
            except FileNotFoundError as err:
                print(err.args)

        if not self.quiet:
            self.blast_message(loaded)

        return

    def poll(self, question, options):
        ''' Starts the polling routines'''
        text = ""
        if self.polling:
            self.blast_message("There is currently a poll running. Try again later!")
            return
        if question:
            text += "Voting started for: %s" % question

        if options:
            text += "\nType vote " + str(options)

        if text is not '':
            text += "\nVoting closes in 3 minutes"

        self.blast_message(text)
        threading.Thread(target=self.start_vote, args=()).start()
        print("gstart_vote_called")


    def vote(self, user, vote):
        ''' Kicks off the voting routines '''
        if self.polling:
            if user in self.voted_list:
                return
            if user is self.bot_user_id:
                return
            vote = vote.strip()
            try:
                self.poll_results[vote] += 1
                self.poll_count += 1
                self.voted_list.append(user)
                self.blast_message("{}'s vote was recorded.".format(self.memlist[user]))
            except KeyError:
                self.blast_message("That wasn't an option, {}".format(self.memlist[user]))

    def blast_message(self, text):
        """ Sends text data to the group
            text message to be sent     - Type: string
        """
        _data = {"text":text, "bot_id": str(self.bot_id)}
        data_json = json.dumps(_data)
        resp = requests.post('https://api.groupme.com/v3/bots/post', data=data_json)
        print(resp.status_code)
        return resp.status_code


    def log_user_message(self, msg):
        """ Stores new message to relevant file"""
        text = ' '.join(msg["text"].split('\n'))+"\n"
        user = msg["user_id"]

        log_f = "data/"+self.memlist[user]+".log"
        with open(log_f, 'a') as log:
            log.write(text)

    def start_vote(self, opts=None):
        ''' Sends remindersfor poll results '''
        if opts or opts is None:
            print("STARTING VOTE")
            threading.Timer(2*60, self.blast_message, ["1 minute remaining!"]).start()
            print("3 minutes left")
            #threading.Timer(60, print, ['4 minutes left']).start()
            #threading.Timer(120, print, ['3 minutes left']).start()
            threading.Timer(180, self.close_vote).start()


    def close_vote(self):
        '''Returns the poll results to the groupchat'''
        text = 'Poll Results'
        for k in self.poll_results.keys():
            pct = float((self.poll_results[k]/self.poll_count))
            text += "\n{}: {:.1%}".format(k, pct)

        self.polling = False
        self.blast_message(text)
